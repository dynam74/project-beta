import React, {useEffect, useState} from 'react'

function AppointmentForm(props) {
    const [customerName, setCustomerName] = useState ('')
    const [appointmentDateTime, setAppointmentDateTime] = useState('')
    const [vin, setVin] = useState ('')
    const [reason, setReason] = useState('')
    const [technicians, setTechnicians] = useState ([])
    const [selectedTechnician, setSelectedTechnician] = useState("");
    const [submitted, setSubmitted] = useState(false);

    const handleCustomerNameChange = (event) => {
        const value = event.target.value
        setCustomerName(value)
      }
    const handleAppointmentDateTimeChange = (event) => {
        const value = event.target.value
        setAppointmentDateTime(value)
      }
    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
      }
    const handleReasonChange = (event) => {
        const value = event.target.value
        setReason(value)
      }


    const handleSelectedTechnicianChange = (event) => {
        const value = event.target.value
        setSelectedTechnician(value)
      }
      useEffect(() => {
		const fetchTechnicians = async () => {
			const url = "http://localhost:8080/api/technicians/";
			const response = await fetch(url);

			if (response.ok) {
				const data = await response.json();
				setTechnicians(data.technicians);
			}
		};
		fetchTechnicians();
	}, []);

   
    const handleSubmit = async (event) => {
        event.preventDefault();
        
        const data = {finished: "False"};
        data.customer_name = customerName;
        data.appointment_date_time= appointmentDateTime;
        data.vin = vin;
        data.reason= reason;
        data.technician= selectedTechnician;

        const appointmentUrl = `http://localhost:8080/api/appointments/`;
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const appointmentResponse = await fetch(appointmentUrl, fetchConfig);
        if (appointmentResponse.ok) {
            const newAppointment = await appointmentResponse.json();

            setCustomerName("");
            setAppointmentDateTime("");
            setVin("");
            setReason("");
            setSelectedTechnician("");
            setSubmitted(true);
            props.fetchAppointments();
            props.fetchTechnician();
            props.fetchCustomer();

        }
    }
        
return(
    <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1 id="appHeader">Create A Service Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
    
                        <div className="form-floating mb-3">
                            <input onChange={handleCustomerNameChange} value={customerName} placeholder="Customer Name" required type="text" name="customer name" id="customer name" className="form-control" />
                            <label htmlFor="customer came">Customer Name</label>                     
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleAppointmentDateTimeChange} value={appointmentDateTime} placeholder="Date/Time" required type="datetime-local" name="date_time" id="date_time" className="form-control" />
                            <label htmlFor="date_time">Date/Time</label>                     
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="vin" required type="vin" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">Vin</label>                     
                        </div>
                        <div className="mb-3">
                            <select onChange={(e)=>setSelectedTechnician(e.target.value)} required id="technician" name="technician" className="form-select">
                                <option value="">Select Your Technician</option>
                                {technicians.map(technician => {
                                    return (
                                        <option key={technician.id} value={technician.id}>{technician.name}</option>
                                    );
                                })}
                            </select>
                        </div>       
                        <div className="form-floating mb-3">
                            <input onChange={handleReasonChange} value={reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                            <label htmlFor="reason">Reason for Appointment</label>                     
                        </div>            
                        <button variant="contained" size="medium" style={{backgroundColor:"black",
                       fontWeight:"normal", color:"white" }} className="btn btn" id="appBtn">Reserve the Appointment</button>
                    </form>
                    <p></p>
                    {submitted && (
                        <div className="alert alert-success mb-0" id="success-message">
                            Your appointment has been successfully created. We'll see you soon!
                        </div>
                    )}
                </div>
            </div>
        </div>
);
}


export default AppointmentForm;
