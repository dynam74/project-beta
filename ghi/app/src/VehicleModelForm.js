import React, {useState, useEffect} from "react";

function ModelForm(props){
    const[name, setName]= useState('')
    const[pictureUrl, setPictureUrl]= useState('')
    const[manufacturers, setManufacturers]=useState([])
    const [selectedManufacturer, setSelectedManufacturer] = useState("");
    const [submitted, setSubmitted] = useState(false);

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
      }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)
      }
    const handleSelectedManufacturerChange = (event) => {
        const value = event.target.value
        setSelectedManufacturer(value)
      }
      useEffect(() => {
        const fetchManufacturers = async () => {
            const url = "http://localhost:8100/api/manufacturers/";
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setManufacturers(data.manufacturers);
            }
        };
        fetchManufacturers();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.manufacturer_id = selectedManufacturer;
        data.picture_url = pictureUrl;


        
        const modelUrl = `http://localhost:8100/api/models/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            const newVehicle = await response.json();
 

            setName("");
            setSelectedManufacturer("");
            setPictureUrl("");
            setSubmitted(true);
            props.fetchModels();

        }
    
    }
          return(
            <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create a vehicle model</h1>
                            <form onSubmit={handleSubmit} id="create-model-form">
                                <div className="form-floating mb-3">
                                    <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                    <label htmlFor="name">Name</label>                     
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="Picture" required type="text" name="picture_url" id="picture_url" className="form-control" />
                                    <label htmlFor="picture_url">Picture URL</label>                     
                                </div>
                                <div className="mb-3">
                                    <select onChange={(e)=>setSelectedManufacturer(e.target.value)} required id="manufacturer" name="manufacturer" className="form-select">
                                        <option value="">Choose a manufacturer</option>
                                        {manufacturers.map(manufacturer => {
                                            return (
                                                <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                            );
                                        })}
                                    </select>
                                  </div>                    
                                <button variant="contained" size="medium" style={{backgroundColor:"black",
                              fontWeight:"normal", color:"white" }} className="btn btn">Create</button>
                            </form>
                            <p></p>
                            {submitted && (
                                <div className="alert alert-success mb-0" id="success-message">
                                    Model has been successfully created. Thank you!
                                </div>
                            )}
                        </div>
                    </div>
                </div>
        );
    }


export default ModelForm;


