import React, {useEffect, useState} from 'react'

function CustomerForm(props) {
    const [name, setName] = useState('');
    const [submitted, setSubmitted] = useState(false);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [address, setAddress] = useState('');
    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    }

    const [phoneNumber, setPhoneNumber] = useState('');
    const handlePhoneNumberChange = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.address = address;
        data.phone_number = phoneNumber

        const salesPersonUrl = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(salesPersonUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            setSubmitted(true);
            setName("");
            setAddress("");
            setPhoneNumber("");
            props.fetchCustomer();



        }
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Customer!</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={handleNameChange} placeholder="Fabric" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={address} onChange={handleAddressChange} placeholder="Fabric" required type="text" name="address" id="address" className="form-control"/>
                <label htmlFor="address">Address</label>
              </div><div className="form-floating mb-3">
                <input value={phoneNumber} onChange={handlePhoneNumberChange} placeholder="Fabric" required type="text" name="phone_number" id="phone_number" className="form-control"/>
                <label htmlFor="phone_number">Phone number</label>
              </div>
              <button variant="contained" size="medium" style={{backgroundColor:"black",
            fontWeight:"normal", color:"white" }} className="btn btn">Create</button>
            </form>
            <p></p>
                    {submitted && (
                        <div className="alert alert-success mb-0" id="success-message">
                            Customer has been successfully created. Thank you!
                        </div>
                    )}
          </div>
        </div>
      </div>

    )
}

export default CustomerForm
