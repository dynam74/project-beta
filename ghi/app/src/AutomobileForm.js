import React, { useState, useEffect } from "react";

function AutomobileForm(props){
    const[model, setModel]=useState('')
    const[year, setYear] = useState("")
    const[color, setColor] = useState("")
    const[vin, setVin]= useState("")
    const [models, setModels] = useState([]);
    const [submitted, setSubmitted] = useState(false);
    const [existed, setExisted] = useState(false);

    const handleModelChange = (event) => {
        const value = event.target.value
        setModel(value)
      }
      const handleYearChange = (event) => {
        const value = event.target.value
        setYear(value)
      }
      const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
      }
      const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
      }
      const fetchData = async () => {
			const url = "http://localhost:8100/api/models/";
			const response = await fetch(url);

			if (response.ok) {
				const data = await response.json();
				setModels(data.models);
			}
		}
		useEffect(() => {
            fetchData();
    
	}, []);

      const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.model_id = model;
        data.year= year;
        data.color = color;
        data.vin =vin

        const url = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {

            setModel([]);
            setYear("");
            setColor("");
            setVin("");
            setSubmitted(true);
            setExisted(false);
            props.fetchAutomobiles();
        } else {
            setSubmitted(false);
            setExisted(true);
        }

 

        }
          return(
            <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Create a new Automobile</h1>
                            <form onSubmit={handleSubmit} id="create-Auto-form">
                                <div className="mb-3">
                                    <select onChange={handleModelChange} value={model} required id="model" name="model" className="form-select">
                                        <option value="">Choose the Vehicle</option>
                                        {models.map(model => {
                                            return (
                                                <option key={model.id} value={model.id}>{model.name}</option>
                                            );
                                        })}
                                    </select>
                                </div>  
                                <div className="form-floating mb-3">
                                    <input onChange={handleYearChange} value={year} placeholder="year" required type="text" name="year" id="year" className="form-control" />
                                    <label htmlFor="year">Year</label>                     
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={handleColorChange} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                                    <label htmlFor="color">Color</label>                     
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                                    <label htmlFor="vin">VIN</label>                     
                                </div>
                  
                                <button variant="contained" size="medium" style={{backgroundColor:"black",
                                fontWeight:"normal", color:"white" }} className="btn btn" id="autoCreateBtn">Create</button>
                            </form>
                            <p></p>
                            {submitted && (
                                <div className="alert alert-success mb-0" id="success-message">
                                    Your automobile has been successfully created. Thank you!
                                </div>
                            )}
                            {existed && (
                                <div className="alert alert-danger mb-0" id="error-message">
                                    Please enter another VIN number. Thank you!
                                </div>
                            )}
                        </div>
                    </div>
                </div>
        );
    };


export default AutomobileForm;


