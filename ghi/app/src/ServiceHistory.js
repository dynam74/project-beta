import React, { useEffect, useState} from 'react'

function AppointmentHistory() {
    const [appointments, setAppointments] = useState([])
    const [vin, setVin] = useState('')

    const handleVinChange = async (event) => {
        const value = event.target.value
        setVin(value)
    }
    function convertToDate(dateTimeObject) {
        return new Date(dateTimeObject)
      }
    
    
    async function handleSearch() {
        const url = "http://localhost:8080/api/appointments"
        const response = await fetch(url)
        const data = await response.json()
        const appointmentData = data.appointments
        const result = appointmentData.filter(appointment => appointment.vin === vin)
        setAppointments(result);
        if (result.length === 0) {
            alert("Service was canceled or VIN number was not found(!Please make sure there is no space after VIN number!). Please enter another VIN. Thank you!")
    }

}

    
    return (
        <>
        <div className="input-group">
            <input onChange={handleVinChange} type="search" value={vin} className="form-control rounded" placeholder="Search VIN" aria-label="Search" aria-describedby="search-addon" />
            <button variant="contained" size="medium" style={{backgroundColor:"black",
            fontWeight:"normal", color:"white" }} onClick={handleSearch} >Search VIN</button>
        </div>
            <h1>Service History</h1>
                <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>VIP</th>
                        <th>Finished</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                    return (
                        <tr key={appointment.id}>
                        <td>{ appointment.vin }</td>
                        <td>{ appointment.customer_name }</td>
                        <td>{convertToDate(appointment.appointment_date_time).toDateString()}</td>
                        <td>{convertToDate(appointment.appointment_date_time).toLocaleTimeString()}</td>
                        <td>{ appointment.technician }</td>
                        <td>{ appointment.reason }</td> 
                        <td>{ appointment.vip ? "Yes" : "No"}</td>
                        <td>{ appointment.finished ? "Yes" : "No"}</td>
                        </tr>
                    );
                    })}
                </tbody>
                </table>
        </>
    )
}
export default AppointmentHistory
