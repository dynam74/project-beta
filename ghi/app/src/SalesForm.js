import React, {useEffect, useState} from 'react'

function SalesForm(props) {
    const [automobile, setAutomobile] = useState('');
    const handlAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const [sales_person, setSalesPerson] = useState('');
    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        setSalesPerson(value);
    }

    const [customer, setCustomer] = useState('');
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const [price, setPrice] = useState('');
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.automobile = automobile;
        data.sales_person = sales_person;
        data.customer = customer;
        data.price = price

        const salesPersonUrl = "http://localhost:8090/api/salesrecords/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(salesPersonUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
            const data = await response.json();
            console.log(data)

            setAutomobile("");
            setSalesPerson("");
            setCustomer("");
            setPrice("");
            props.fetchSales();
            props.fetchAutomobile();
            props.fetchSalesPerson();
            props.fetchCustomer();



        }
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a sales record</h1>
                    <form onSubmit={handleSubmit} id="create-sales-form">
                        <div className="mb-3">
                            <select value={automobile} onChange={handlAutomobileChange} required id="autmobile" name="automobile" className="form-select">
                                <option value="">Choose an Automobile</option>
                                {props.automobile.map(auto => {
                                    if (auto.availability===true) {
                                        return (
                                            <option key={auto.import_href} value={auto.import_href}>
                                            {auto.vin}
                                            </option>
                                        );
                                    }

                                    })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select value={sales_person} onChange={handleSalesPersonChange} required id="sales_person" name="sales_person" className="form-select">
                                <option value="">Choose a Sales Person</option>
                                {props.salesperson.map(salesp => {
                                        return (
                                            <option key={salesp.id} value={salesp.name}>
                                                {salesp.name}
                                            </option>
                                        );
                                    })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select value={customer} onChange={handleCustomerChange} required id="customer" name="customer" className="form-select">
                                <option value="">Choose a Customer</option>
                                {props.customer.map(cust => {
                                        return (
                                            <option key={cust.id} value={cust.name}>
                                                {cust.name}
                                            </option>
                                        );
                                    })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={price} onChange={handlePriceChange} placeholder="Price" required type="number" name="price" id="price" className="form-control"/>
                            <label htmlFor="price">Price</label>
                        </div>
                    <button variant="contained" size="medium" style={{backgroundColor:"black",
                   fontWeight:"normal", color:"white" }}  className="btn btn">Create</button>
                  </form>
                </div>
            </div>
        </div>
    );
    }


export default SalesForm
