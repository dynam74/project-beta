import React, {useEffect, useState} from 'react'

function TechnicianForm(props){
    const [name, setName] = useState('')
    const [employeeNumber, setEmployeeNumber] = useState ('')
    const [submitted, setSubmitted] = useState(false);
    
    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
      }
    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value
        setEmployeeNumber(value)
      }
    const handleSubmit = async (event) => {
        event.preventDefault();
        
        const data = {};
        data.name = name;
        data.employee_number= employeeNumber;
        
        const technicianUrl = `http://localhost:8080/api/technicians/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            setSubmitted(true);
            setName("");
            setEmployeeNumber("");
            props.fetchTechnician()

        }
        else {
            setSubmitted(false);
        }
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeNumberChange} value={employeeNumber} placeholder="Id" required type="text" name="id" id="id" className="form-control" />
                            <label htmlFor="id">Employee Number</label>
                        </div>
                        <button variant="contained" size="medium" style={{backgroundColor:"black",
                       fontWeight:"normal", color:"white" }} className="btn btn">Create</button>
                    </form>
                    <p></p>
                        {submitted && (
                            <div className="alert alert-success mb-0" id="success-message">
                                Technician has been successfully created. Thank you!
                            </div>
                        )}
                </div>
            </div>
        </div>
    );
}


export default TechnicianForm

