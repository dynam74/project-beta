from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
from service_rest.models import Technician, Appointment, AutomobileVO
from django.views.decorators.http import require_http_methods
import json


# Create your views here.
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin"
        "import_href",
        "id",
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "name",
        "employee_number",
    ]


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer_name",       
        "appointment_date_time",     
        "reason",  
        "vip",
        "finished",
        "canceled",
        "id",
    ]
    encoders = {
        "technician": TechnicianEncoder,
    }

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "customer_name",       
        "appointment_date_time",     
        "technician", 
        "reason",  
        "vip",
        "finished",
        "canceled",
        "id",
    ]

    def get_extra_data(self, o):
        return {"technician": o.technician.name}


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        technicians = Technician.objects.get(id=content["technician"])
        content["technician"] = technicians
        try:
            AutomobileVO.objects.get(vin=content["vin"])
            content["vip"] = True
        except AutomobileVO.DoesNotExist: 
            content["vip"] = False
    appointments = Appointment.objects.create(**content)
    return JsonResponse(
        appointments,
        AppointmentListEncoder,
        safe=False,
    )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        appointments = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointments,
            AppointmentListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        try:
            if "appointments" in content:
                appointments = Appointment.objects.get(id=content["appointments"])
                content["appointments"] = appointments
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"}
            )
        Appointment.objects.filter(id=pk).update(**content)
        appointments = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointments,
            AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Could not input technician"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_technician(request, pk):
    if request.method == "DELETE":
        try:
            count, _ = Technician.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician Does not exist"})

    elif request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    else:
        content = json.loads(request.body)
        Technician.objects.filter(id=pk).update(**content)
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician, 
            encoder=TechnicianEncoder, 
            safe=False,
            )
