from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)

    def __str__(self):
        return self.vin

class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.name
    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk": self.id})


class Appointment(models.Model):
    customer_name = models.CharField(max_length=200)
    vin = models.CharField(max_length=17)
    vip = models.BooleanField(default=False)
    appointment_date_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    reason = models.TextField()
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )
    finished = models.BooleanField(default=False)
    canceled = models.BooleanField(default=False)
    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.id})

    def __str__(self):
        return f'{str(self.customer_name), str(self.vip)}'

