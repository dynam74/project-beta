# CarCar

Team:

* Daniel Nam - Sales Microservice
* Vivi Nguyen - Service Microservice

## Design
CarCar is application that is built on a Django backend, React frontend, and utilizes PostgreSQL as a database.

The purpose of this application is to serve as an automobile dealership that includes features such as invetory management, sales data/management, and service management. Each of these features are categorized into microservices that utilize RESTful APIs in order to send data to one another.

**Set up**
- Clone the repo using this link:`https://gitlab.com/dynam74/project-beta.git`
- Download Docker Desktop if not already installed and open
- Make sure you are in your project directory in your terminal window
- Run the commands below:
    - `docker volume create beta-data`
    - `docker-compose build`
    - `docker-compose up`
- Wait 2-5 minutes as it takes a little bit of time for react to start up. Be sure your servers are running
- Once you are sure your servers are running, go to `localhost:3000` to use the website.

**Context Map**
![context map](/img/contextmapcarcar.png)

**URLs for API client**

Manufacturers
| Action  | Method  | URL  | JSON (if needed)  |
|---|---|---|---|
| List manufacturers	  |  GET | http://localhost:8100/api/manufacturers/  |   |
| Create a manufacturer	  | POST  | http://localhost:8100/api/manufacturers/  | { "name": "Chrysler" }  |
| Get a specific manufacturer  | GET  | http://localhost:8100/api/manufacturers/:id/  |   |
| Update a specific manufacturer  | PUT  | http://localhost:8100/api/manufacturers/:id/  |   |
| Delete a specific manufacturer  | DELETE  | http://localhost:8100/api/manufacturers/:id/  |   |

Vehicles
| Action  | Method  | URL  | JSON (if needed)  |
|---|---|---|---|
| List vehicle models	  |  GET | http://localhost:8100/api/models/  |   |
| Create a vehicle model	  | POST  | 	http://localhost:8100/api/models/  | { "name": "Chrysler" }  |
| Get a specific manufacturer  | GET  | http://localhost:8100/api/manufacturers/:id/  |  {"name": "Sebring",   "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg", "manufacturer_id": 1 }  |
| Update a specific vehicle model  | PUT  | http://localhost:8100/api/models/:id/  |   |
| Delete a specific vehicle model  | DELETE  | http://localhost:8100/api/models/:id/  |   |

Automobile
| Action  | Method  | URL  | JSON (if needed)  |
|---|---|---|---|
| List automobiles		  |  GET | http://localhost:8100/api/automobiles/  |   |
| Create an automobile	  | POST  | http://localhost:8100/api/manufacturers/  | {"color": "red", "year": 2012, "vin": "1C3CC5FB2AN120174" "model_id": 1}  |
| Get a specific automobile  | GET  | http://localhost:8100/api/automobiles/:vin/  |   |
| Update a specific automobile  | PUT  | 	http://localhost:8100/api/automobiles/:vin/  |   |
| Delete a specific automobile  | DELETE  | http://localhost:8100/api/automobiles/:vin/  |   |

Sales
| Action  | Method  | URL  | JSON (if needed)  |
|---|---|---|---|
| List Sales Person  | GET  | http://localhost:8090/api/salesperson/  |   |
| Create Sales Person  | POST  | http://localhost:8090/api/salesperson/  |  {"name": "vivi nguyennn", "employee_number": "123476"} |
| List Customer  | GET  | http://localhost:8090/api/customers/  |   |
| Create a Customer  | POST  | http://localhost:8090/api/customers/  | {"name": "Daniel Nam","address": "914 elmo street", "phone_number": "12941029570"}  |
| List sales records  | GET  | http://localhost:8090/api/salesrecords/  |   |
| Create salesrecord | POST  |  http://localhost:8090/api/salesrecords/ |  {"automobile": "/api/automobiles/1C3CC5FB2AN120174/","customer": "Andy Lam","sales_person": "Daniel nam","price": 100} |


Services
| Action  | Method  | URL  | JSON (if needed)  |
|---|---|---|---|
| List Technicians  | GET  | http://localhost:8080/api/technicians/  |   |
| Create a Technician  | POST  | http://localhost:8080/api/technicians/  |  {"name": "vivi nguyennn", "employee_number": "695"} |
| Get Technician Detail  | GET  | http://localhost:8080/api/technicians/pk  |   |
| Delete Technician | DEL | http://localhost:8080/api/technicians/id  |   |
| Update Technician Detail  | PUT  | http://localhost:8080/api/technicians/id  | {"id":"1","name":"dede de","employee_number":"543"} |
| Create an Appointment"  | POST  | http://localhost:8080/api/appointments/  | {"vin": "1C3CC5FB2AN120544","customer_name": "ty ngy","appointment_date_time":"2023-02-26 08:23","reason":"windows", "finished":false,"canceled":false,"technician": 1} |
| Update Appointment  | PUT  | http://localhost:8080/api/appointments/id  | {"vip":true } |
| Appointments List | GET  |  http://localhost:8080/api/appointments/ |   |
| Appointments Detail | GET  |  http://localhost:8080/api/appointments/id |   |


## Service microservice
![Service Models](/img/ServiceModels.png)


The service microservice has 3 different backend models: AutomobileVO, Technician, and Appointment.  The AutomobileVO model has 2 attributes: vin and import_href. The Technician model has 2 attributes: name and employee_number. The Appointment model has 8 attributes: customer_name, technician, appointment_date_time, reason, vip, vin, finished and canceled. The Automobile value object contains the vehicle VIN number that is immutable - this data is retrieved from the Automobile model in the Inventory microservice via a polling function("poller.py"). The retrieved VIN number is used to determine the VIP status. Users can update current service appointments to "finished" or "canceled", create new technicians, schedule new appointments, and view upcoming scheduled appointments. Users can also search for the vehicle's service history using its VIN number.

## Sales microservice

![Sales Models](/img/salesmodels.png)

The sales microservice has 4 different models: AutomobileVO, SalesPerson, Customer, and SalesRecord. The main model in this microservice is the Salesrecord as the purpose of this microservice is to be able to create and track sales that are being made on the website. In order to create a SalesRecord, the model requires data from Customers, SalesPersons, and Automobiles, which is retreived via a Foreign key to the other models. Addtionally, a fourth property, price, is added to the SalesRecord model.

It is important to note that the sales microservices needed an Automobile value object taken from the inventory microservice via a poller, since the automobile data is not stored within the sales microservice. This data is necessary as the salesrecord needs to record which car (specified by VIN) was sold in any given instance.


Here is a breakdown of what you are able to do with this microservice:

- Create a sales person
- Create a customer
- Create a Sales Record
- View all Sales Records
- View the Sales Record of a specific sales person
