from django.shortcuts import render
from common.json import ModelEncoder
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, SalesPerson, Customer, SalesRecord

# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
        "year",
        "color",
        "availability"
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
        "id"
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
        "id"
    ]

class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "sales_person",
        "automobile",
        "customer",
        "price",
        "id"
    ]
    def get_extra_data(self, o):
        return {
            "automobile": o.automobile.vin,
            "sales_person": {
                 "name" :o.sales_person.name,
                 "emp_no":o.sales_person.employee_number,
            },
            "customer": o.customer.name
        }

    encoders = {
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
        "automobile": AutomobileVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        sales_persons = SalesPerson.objects.all()
        return JsonResponse(
            {"salespersons": sales_persons},
            encoder = SalesPersonEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder = SalesPersonEncoder,
                safe = False
            )
        except:
            return JsonResponse(
                {"message": "Invalid salesperson"},
                status=500
            )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_salesperson(request, id):
    if request.method == "GET":
        try:
            sales_persons = SalesPerson.objects.get(id=id)
            return JsonResponse(
                sales_persons,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Does no exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        count, _ = SalesPerson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else: #PUT
        content = json.loads(request.body)
        SalesPerson.objects.filter(id=id).update(**content)
        sales_persons = SalesPerson.objects.get(id=id)
        return JsonResponse(
            sales_persons,
            encoder=SalesPersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder = CustomerEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder = CustomerEncoder,
                safe = False
            )
        except:
            return JsonResponse(
                {"message": "Invalid customer"},
                status=500
            )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_customer(request, id):
    if request.method == "GET":
        try:
            customers = Customer.objects.get(id=id)
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does no exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else: #PUT
        content = json.loads(request.body)
        Customer.objects.filter(id=id).update(**content)
        sales_persons = Customer.objects.get(id=id)
        return JsonResponse(
            sales_persons,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_salesrecords(request):
    if request.method == "GET":
        salesrecord = SalesRecord.objects.all()
        return JsonResponse(
            {"salesrecords": salesrecord},
            encoder = SalesRecordEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            if automobile.availability is True:
                content["automobile"] = automobile

                customer_name = content["customer"]
                customer = Customer.objects.get(name=customer_name)
                content["customer"] = customer

                sales_person = content["sales_person"]
                salesperson = SalesPerson.objects.get(name=sales_person)
                content["sales_person"] = salesperson

                automobile.availability = False
                automobile.save()

                record = SalesRecord.objects.create(**content)
                return JsonResponse(
                    record,
                    encoder=SalesRecordEncoder,
                    safe=False,
                )
            else:
                response = JsonResponse(
                    {"message": "Sorry! Not available."},
                )
                response.status_code = 400
                return response
        except:
            response = JsonResponse(
                {"message": "Could not create sales record"},
            )
            response.status_code = 400
            return response

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_salesrecord(request, id):
    if request.method == "GET":
        try:
            sales_persons = SalesRecord.objects.get(id=id)
            return JsonResponse(
                sales_persons,
                encoder=SalesRecordEncoder,
                safe=False
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "Does no exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        count, _ = SalesRecord.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else: #PUT
        content = json.loads(request.body)
        try:
            content = json.loads(request.body)
            salesrecord = SalesRecord.objects.get(id=id)

            automobile_href = content["automobile"]
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            content["automobile"] = automobile

            customer_name = content["customer"]
            customer = Customer.objects.get(name=customer_name)
            content["customer"] = customer

            sales_person = content["sales_person"]
            salesperson = SalesPerson.objects.get(name=sales_person)
            content["sales_person"] = salesperson

            props = ["sales_person", "customer", "automobile", "price"]
            for prop in props:
                if prop in content:
                    setattr(salesrecord, prop, content[prop])
            salesrecord.save()
            return JsonResponse(
                salesrecord,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET"])
def api_list_automobiles(request):
    if request.method == "GET":
        cars = AutomobileVO.objects.all()
        return JsonResponse(
            cars,
            encoder=AutomobileVOEncoder,
            safe=False,
        )
