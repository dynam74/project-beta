from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=300, unique=True)
    vin = models.CharField(max_length=100)
    year = models.PositiveSmallIntegerField()
    color = models.CharField(max_length=50)
    availability = models.BooleanField(default=True)


    def __str__(self):
        return self.vin


class SalesPerson(models.Model):
    name = models.CharField(max_length=200, unique=True)
    employee_number = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.name

class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name

class SalesRecord(models.Model):
    sales_person = models.ForeignKey(
        SalesPerson, related_name = "sales", on_delete=models.PROTECT
    )
    automobile = models.ForeignKey(
        AutomobileVO, related_name = "sales", on_delete=models.PROTECT
    )
    customer = models.ForeignKey(
        Customer,related_name = "sales",on_delete=models.PROTECT
    )
    price = models.CharField(max_length=50)


    def __str__(self):
        return f"{self.sales_person} {self.automobile} {self.customer}"
